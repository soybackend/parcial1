describe('Test over page web los estudiantes', function() {

context('Login and register test', function(){
  beforeEach(function(){
    cy.visit('https://losestudiantes.co')
    cy.contains('Cerrar').click()
    cy.contains('Ingresar').click()
  })

  it('Login Succes', function() {
    cy.get('.cajaLogIn').find('input[name="correo"]').click().type("oh.urrego@uniandes.edu.co")
    cy.get('.cajaLogIn').find('input[name="password"]').click().type("Avril112")
    cy.get('.cajaLogIn').contains('Ingresar').click()
    cy.contains('Ingresar').should('not.exist')
  })

  it('Login with invalid password and email', function() {
    cy.get('.cajaLogIn').find('input[name="correo"]').click().type("invalidemail@example.com")
    cy.get('.cajaLogIn').find('input[name="password"]').click().type("3333")
    cy.get('.cajaLogIn').contains('Ingresar').click()
    cy.contains('El correo y la contraseña que ingresaste no figuran en la base de datos. Intenta de nuevo por favor.')
  })

  it('Login without password', function(){
    cy.get('.cajaLogIn').find('input[name="correo"]').click().type("invalidemail@example.com")
    cy.get('.cajaLogIn').contains('Ingresar').click()
    cy.contains('Ingresa una contraseña')
  })

  it('Password reset without email', function(){
    cy.get('.cajaLogIn').contains('Recuperar contraseña').click()
    cy.get('.cajaLogIn').contains('Recuperar').click()
    cy.contains('Error: Ingresa un correo')
  })

  it('Password reset with wrong email', function(){
    cy.get('.cajaLogIn').contains('Recuperar contraseña').click()
    cy.get('.cajaLogIn').find('input[name="correo"]').click().type("invalidemail@example.com")
    cy.get('.cajaLogIn').contains('Recuperar').click()
    cy.contains('Error: No existe una cuenta para ese correo')
  })

  it('Create account with existing user', function() {
    cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("Oscar")
    cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("Urrego")
    cy.get('.cajaSignUp').find('input[name="correo"]').click().type("oh.urrego@uniandes.edu.co")
    cy.get('.cajaSignUp').get('.no_bold').find('input[type="checkbox"]').check()
    cy.get('.cajaSignUp').find('select[name="idPrograma"]').select('Maestría en Ingeniería de Software')
    cy.get('.cajaSignUp').find('input[name="password"]').click().type("Avril113")
    cy.get('.cajaSignUp').find('input[name="acepta"]').check()
    cy.contains('Registrarse').click()
    cy.contains("Error: Ya existe un usuario registrado con el correo 'oh.urrego@uniandes.edu.co'")
  })

  it('Create account with invalid email', function(){
    cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("Oscar")
    cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("Urrego")
    cy.get('.cajaSignUp').find('input[name="correo"]').click().type("oh.urrego")
    cy.get('.cajaSignUp').get('.no_bold').find('input[type="checkbox"]').check()
    cy.get('.cajaSignUp').find('select[name="idPrograma"]').select('Maestría en Ingeniería de Software')
    cy.get('.cajaSignUp').find('input[name="password"]').click().type("Avril113")
    cy.get('.cajaSignUp').find('input[name="acepta"]').check()
    cy.contains('Registrarse').click()
    cy.contains("Ingresa un correo valido")
  })

  it('Create account without email', function(){
    cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("Oscar")
    cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("Urrego")
    cy.get('.cajaSignUp').get('.no_bold').find('input[type="checkbox"]').check()
    cy.get('.cajaSignUp').find('select[name="idPrograma"]').select('Maestría en Ingeniería de Software')
    cy.get('.cajaSignUp').find('input[name="password"]').click().type("Avril113")
    cy.get('.cajaSignUp').find('input[name="acepta"]').check()
    cy.contains('Registrarse').click()
    cy.contains("Ingresa tu correo")
  })

  it('Create account without password', function(){
    cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("Oscar")
    cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("Urrego")
    cy.get('.cajaSignUp').find('input[name="correo"]').click().type("oh.urrego@uniandes.edu.co")
    cy.get('.cajaSignUp').get('.no_bold').find('input[type="checkbox"]').check()
    cy.get('.cajaSignUp').find('select[name="idPrograma"]').select('Maestría en Ingeniería de Software')
    cy.get('.cajaSignUp').find('input[name="acepta"]').check()
    cy.contains('Registrarse').click()
    cy.contains("Ingresa una contraseña")
  })

  it('Create account with invalid password', function(){
    cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("Oscar")
    cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("Urrego")
    cy.get('.cajaSignUp').find('input[name="correo"]').click().type("oh.urrego@uniandes.edu.co")
    cy.get('.cajaSignUp').get('.no_bold').find('input[type="checkbox"]').check()
    cy.get('.cajaSignUp').find('select[name="idPrograma"]').select('Maestría en Ingeniería de Software')
    cy.get('.cajaSignUp').find('input[name="password"]').click().type("Avril")
    cy.get('.cajaSignUp').find('input[name="acepta"]').check()
    cy.contains('Registrarse').click()
    cy.contains("La contraseña debe ser al menos de 8 caracteres")
  })

  it('Create account without accept terms and conditions', function(){
    cy.get('.cajaSignUp').find('input[name="nombre"]').click().type("Oscar")
    cy.get('.cajaSignUp').find('input[name="apellido"]').click().type("Urrego")
    cy.get('.cajaSignUp').find('input[name="correo"]').click().type("oh.urrego@uniandes.edu.co")
    cy.get('.cajaSignUp').get('.no_bold').find('input[type="checkbox"]').check()
    cy.get('.cajaSignUp').find('select[name="idPrograma"]').select('Maestría en Ingeniería de Software')
    cy.get('.cajaSignUp').find('input[name="password"]').click().type("Avril113")
    cy.contains('Registrarse').click()
    cy.contains("Debes aceptar los términos y condiciones")
  })


})
context('Navigate into aplication', function(){

  beforeEach(function(){
    cy.visit('https://losestudiantes.co')
    cy.contains('Cerrar').click()
    cy.get('.buscador').find('div[class="Select-placeholder"]').click()
  })

  it('Teacher Search Test', function() {
    cy.get('.buscador').find('div[class="Select-input"]').get('input').click({force:true}).type('Camilo Sanabria', {force:true})
    cy.get('.Select-menu-outer').should('be.visible')
    cy.wait(2000)
    cy.get('.Select-menu-outer').should('contain', "Camilo Sanabria")
  })

  it('Teacher Page Test', function() {
    cy.get('.buscador').find('div[class="Select-input"]').get('input').click({force:true}).type('Camilo Sanabria', {force:true})
    cy.get('.Select-menu-outer').contains('Camilo Sanabria').click()
    cy.contains('Camilo Sanabria')
  })

  it('Qualify a teacher without logging', function() {
    cy.get('.buscador').find('div[class="Select-input"]').get('input').click({force:true}).type('Camilo Sanabria', {force:true})
    cy.get('.Select-menu-outer').contains('Camilo Sanabria').click()
    cy.get('.boton.btn.btn-primary').click()
    cy.contains('Para calificar a este profesor tienes que ingresar...')
  })

})

})