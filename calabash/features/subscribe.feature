Feature: Suscribe and open a feed and removed

  Scenario: As a user I want to subscribe me to a feed
  Given I swipe right
  And I press list item number 6
  And I press view with id "etxtFeedurl"
  And I enter text "https://www.npr.org/rss/podcast.php?id=510307" into field with id "etxtFeedurl"
  And I press the enter button
  And I press "Confirm"
  And I press "Subscribe"
  And I press "Open Podcast"
  Then I should see "Invisibilia"
