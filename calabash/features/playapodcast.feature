Feature: Suscribe and play a podcast

  Scenario: As a user I want to subscribe me and play a podtcast
  Given I swipe right
  And I press list item number 6
  And I press view with id "etxtFeedurl"
  And I enter text "https://www.npr.org/rss/podcast.php?id=510307" into field with id "etxtFeedurl"
  And I press the enter button
  And I press "Confirm"
  And I press "Subscribe"
  And I press "Open Podcast"
  And I press view with id "container"
  And I press view with id "butAction2"
  Then I should see "1.00x"
